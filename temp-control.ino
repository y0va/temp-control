                                                      
#include <OneWire.h>
#include <DallasTemperature.h>
#include <Adafruit_GFX.h>    // Core graphics library
#include <Adafruit_ST7735.h> // Hardware-specific library
#include <SPI.h>
#include <math.h>


// macros from DateTime.h
/* Useful Constants */
#define SECS_PER_MIN  (60UL)
#define SECS_PER_HOUR (3600UL)
#define SECS_PER_DAY  (SECS_PER_HOUR * 24L)

/* Useful Macros for getting elapsed time */
#define numberOfSeconds(_time_) (_time_ % SECS_PER_MIN)  
#define numberOfMinutes(_time_) ((_time_ / SECS_PER_MIN) % SECS_PER_MIN)
#define numberOfHours(_time_) (( _time_% SECS_PER_DAY) / SECS_PER_HOUR)
#define elapsedDays(_time_) ( _time_ / SECS_PER_DAY)


// Data wire is plugged into pin 2 on the Arduino
// Gertenbach:
//#define ONE_WIRE_BUS 2

//Freudi:
#define ONE_WIRE_BUS 5

// Temperature delta °C
#define TEMP_D 5

// Minimal temp inside
#define MIN_IN 3

// Length of Delay in quarterseconds

// one second
//#define INTERVAL 4

// 5 minutes 5 * 60 * 4

#define INTERVAL 1200

// For the breakout, you can use any 2 or 3 pins
// These pins will also work for the 1.8" TFT shield
#define TFT_CS     6
#define TFT_RST    8  // you can also connect this to the Arduino reset
                      // in which case, set this #define pin to 0!
#define TFT_DC     7

// Option 1 (recommended): must use the hardware SPI pins
// (for UNO thats sclk = 13 and sid = 11) and pin 10 must be
// an output. This is much faster - also required if you want
// to use the microSD card (see the image drawing example)
Adafruit_ST7735 tft = Adafruit_ST7735(TFT_CS,  TFT_DC, TFT_RST);

// Option 2: use any pins but a little slower!
#define TFT_SCLK 13   // set these to be whatever pins you like!
#define TFT_MOSI 11   // set these to be whatever pins you like!
//Adafruit_ST7735 tft = Adafruit_ST7735(TFT_CS, TFT_DC, TFT_MOSI, TFT_SCLK, TFT_RST);

// Setup a oneWire instance to communicate with any OneWire devices (not just Maxim/Dallas temperature ICs)
OneWire oneWire(ONE_WIRE_BUS);

// Pass our oneWire reference to Dallas Temperature. 
DallasTemperature sensors(&oneWire);


float temp_inside, temp_outside, ytop, ybottom, yfactor;

float temp_inside_min=100;
float temp_inside_max=-100;

float temp_outside_min=100;
float temp_outside_max=-100;

long time_min=0;
long time_max=0;
long time_fan=0;

long tcounter=0,counter=0; 

// Pin 
int RELAY1=4;

void setup(void)
{

  tft.initR(INITR_BLACKTAB);
  // start serial port
  Serial.begin(9600);
  Serial.println("Dallas Temperature IC Control Library Demo");

  // Start up the library
  sensors.begin(); // IC Default 9 bit. If you have troubles consider upping it 12. Ups the delay giving the IC more time to process the temperature measurement
  tft.fillScreen(ST7735_BLACK);
  tft.setRotation(tft.getRotation()+1);
  
  // Init Relay pins
  pinMode(RELAY1, OUTPUT);  
  
}


void loop(void)
{ 
  // call sensors.requestTemperatures() to issue a global temperature 
  // request to all devices on the bus
  Serial.print("Requesting temperatures...");
  sensors.requestTemperatures(); // Send the command to get temperatures
  Serial.println("DONE");
  // Switch here if sensors not correct!
  temp_inside=sensors.getTempCByIndex(1);
  temp_outside=sensors.getTempCByIndex(0);

  if (counter>=tft.width() || counter==0) {

    counter=0;

  /*  temp_inside_min=100;
    temp_inside_max=-100;
    temp_outside_min=100;
    temp_outside_max=-100;*/

    tft.fillRect(0,tft.height()/2,tft.width(),tft.height(),ST7735_BLACK);
    drawlines();
  }
    
  if (temp_inside < temp_inside_min) {
    temp_inside_min = temp_inside;
    time_min=counter;
  }
  
  if (temp_inside > temp_inside_max) {
    temp_inside_max = temp_inside;
    time_max=counter;
  }
  
  if (temp_outside < temp_outside_min) {
    temp_outside_min = temp_outside;
//    time_min=counter;
  }
  
  if (temp_outside > temp_outside_max) {
    temp_outside_max = temp_outside;
//    time_max=counter;
  }
  
  
 // tft.fillScreen(ST7735_BLACK);
  tft.fillRect(0,0,tft.width(),tft.height()/2,ST7735_BLACK);
  tft.setCursor(0, 0);
  tft.setTextColor(ST7735_MAGENTA);

  tft.setTextWrap(true);
  tft.print("Temp outside: ");
  tft.println(temp_outside);

  tft.print("Temp inside:  ");
  tft.println(temp_inside);

  tft.print("Tx(in/out): ");
  tft.print(temp_inside_max);
  tft.print("/");
  tft.println(temp_outside_max);
  
  print_time((tcounter-time_max)*INTERVAL/4);
  tft.println(" ago (inside)");  
  
  tft.print("Ti(in/out): ");
  tft.print(temp_inside_min);
  tft.print("/");
  tft.println(temp_outside_min);
  print_time((tcounter-time_min)*INTERVAL/4);
  tft.println(" ago (inside)");    

  ytop = 30; //fmax(temp_inside_max, temp_outside_max);
  ybottom = 0; //fmin(temp_inside_min, temp_outside_min);

  yfactor=(tft.height()/2) /(ytop - ybottom);
  
  tft.drawPixel(counter,tft.height()/2+(ytop - temp_inside )*yfactor, ST7735_CYAN);
  tft.drawPixel(counter,tft.height()/2+(ytop - temp_outside)*yfactor, ST7735_WHITE);

  drawlines();
  
  if (temp_inside > MIN_IN) {
    if (temp_inside > temp_outside + TEMP_D) {
        digitalWrite(RELAY1,LOW); // Turns ON Relays 1
        time_fan++;
        tft.print("Fan on! ");
    } else {  
      digitalWrite(RELAY1,HIGH); // Turns OFF Relays 1
      tft.print("Fan off! ");
    }
  } else {
      digitalWrite(RELAY1,HIGH); // Turns OFF Relays 1
      tft.print("Fan off! ");
  }
  
  print_time(time_fan*INTERVAL/4);
  
 lpDelay(INTERVAL);
 tcounter++;
 counter++;
}

void drawlines() {
  tft.drawLine(0,tft.height()/2, 0, tft.height(), ST7735_MAGENTA);
  tft.drawLine(0,tft.height()-1, tft.width(), tft.height()-1, ST7735_MAGENTA);
  tft.drawLine(0,tft.height()/2+(ytop - 30)*yfactor,tft.width(),tft.height()/2+(ytop - 30)*yfactor,ST7735_RED);
  tft.drawLine(0,tft.height()/2+(ytop - 20)*yfactor,tft.width(),tft.height()/2+(ytop - 20)*yfactor,ST7735_YELLOW);
  tft.drawLine(0,tft.height()/2+(ytop - 10)*yfactor,tft.width(),tft.height()/2+(ytop - 10)*yfactor,ST7735_GREEN);
  tft.drawLine(0,tft.height()/2+(ytop - 5)*yfactor,tft.width(),tft.height()/2+(ytop - 5)*yfactor,ST7735_BLUE);
}

void print_time(long val){  
  int days = elapsedDays(val);
  int hours = numberOfHours(val);
  int minutes = numberOfMinutes(val);
  int seconds = numberOfSeconds(val);

  // digital clock display of current time
  tft.print(days,DEC);  
  printDigits(hours);  
  printDigits(minutes);
  printDigits(seconds);
  
}

void printDigits(byte digits){
  // utility function for digital clock display: prints colon and leading 0
  tft.print(":");
  if(digits < 10)
    tft.print('0');
  tft.print(digits,DEC);  
}


void lpDelay(int quarterSeconds) {
  int oldClkPr = CLKPR;  // save old system clock prescale
  CLKPR = 0x80;    // Tell the AtMega we want to change the system clock
  CLKPR = 0x08;    // 1/256 prescaler = 60KHz for a 16MHz crystal
  delay(quarterSeconds);  // since the clock is slowed way down, delay(n) now acts like delay(n*256)
  CLKPR = 0x80;    // Tell the AtMega we want to change the system clock
  CLKPR = oldClkPr;    // Restore old system clock prescale
}
