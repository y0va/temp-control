# Simple arduino temperature control

Simple arduino based controller that switches on a relay based on the difference of two temperatures

## Hardware used

- [Nanode 5](https://wiki.hackerspaces.org/Nanode)
- [CP2102](https://www.silabs.com/interface/usb-bridges/classic/device.cp2102) from openenergymonitor.org


